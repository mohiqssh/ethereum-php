<?php

namespace Mohiqssh\Ethereum;

class Convertor {

    function bchexdecWei($hex) {
        if (strlen($hex) == 1) {
            return hexdec($hex);
        } else {
            $remain = substr($hex, 0, -1);
            $last = substr($hex, -1);
            $result = bcadd(bcmul(16, $this->bchexdecWei($remain)), hexdec($last));
            return $result;
        }
    }

    function wei2eth($wei) {
        return bcdiv($wei, 1000000000000000000, 8);
    }

}
